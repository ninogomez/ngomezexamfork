﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using Protractor;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace MeWeTestExam
{
    public struct Fields
    { // used for create new fields
        public int id;
        public string PropertyName;
        public string Checklist;
        public string Inspector;
        public string status;

    };


    public class Inspections
    {
        public int id { get; set; }
        public string property { get; set; }
        public string checklist { get; set; }
        public string inspector { get; set; }
        public string status { get; set; }
    }

    [TestClass]
    public class UnitTest1
    {
        
        
        IWebDriver driver;
        string url = "http://localhost:3000";

        [TestInitialize]
        public void SetUp()
        {
            driver = new FirefoxDriver();
            driver.Navigate().GoToUrl(url);

            System.Threading.Thread.Sleep(2000);
            IWebElement Elem = driver.FindElement(By.XPath("//body/nav/ul/li[1]/a"));
            //Console.WriteLine(Elem.Text);

        }


        [TestMethod]
        public void _01_CheckList()
        {
            IWebElement Elem = driver.FindElement(By.XPath("//body/nav/ul/li[1]/a"));

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(100));
            wait.Until(ExpectedConditions.ElementToBeClickable(By.ClassName("menu"))); //huwat sa menu to appear
            int countValue = driver.FindElements(By.XPath("html/body/div[1]/section/ul/li")).Count;

            // //li/h3[1]
            // #1 Activity - check if how many elements.
            Console.WriteLine(countValue);
            if (countValue <= 0)
            {
                Assert.IsTrue(true,"No List");
            }

        }

        [TestMethod]
        public void _02_CreateAndVerify()
        {
            Fields Flds;

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(100));
            driver.FindElement(By.XPath("//a[text()='Create New']")).Click();
            IWebElement Elem = wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//label[@for='property']")));
            System.Threading.Thread.Sleep(2000);
            //CreateNew(Flds, driver);

           
            Flds.PropertyName = "Test" + System.DateTime.Now.ToString("mm:ss");
            //driver.FindElement(NgBy.Model("inspection.property")).Text;
            driver.FindElement(NgBy.Model("inspection.property")).SendKeys(Flds.PropertyName);
            Flds.Checklist = SelectDropDown(driver, "inspection.checklist");
            Flds.Inspector = SelectDropDown(driver, "inspection.inspector");
            Flds.status = SelectDropDown(driver, "inspection.status");
    
            driver.FindElement(By.XPath("//button[@name='SAVE']")).Click();
            System.Threading.Thread.Sleep(3000);

            driver.FindElement(By.LinkText("Home")).Click();
            System.Threading.Thread.Sleep(3000);

            string json = getInspections("new");
             List<Inspections> ListInspect = JsonConvert.DeserializeObject<List<Inspections>>(json);
            int count = ListInspect.Count;
          //  Console.WriteLine(driver.Url);
            //int count =  driver.FindElements(By.XPath("//li/h3[1]")).Count;
            Console.WriteLine(count);
            Flds.id = count;
            AssertMethod(Flds);
            
        }

        [TestMethod]
        public void _03_UpdateAndVerify()
        {
            // //li[16]/h3

            Fields Flds;
            IWebElement Elem = driver.FindElement(By.XPath("//body/nav/ul/li[1]/a"));

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(100));
            wait.Until(ExpectedConditions.ElementToBeClickable(By.ClassName("menu"))); //huwat sa menu to appear
            int totItem = driver.FindElements(By.XPath("//li/h3[1]")).Count;

            Random rnd = new Random();
            int ranNum = rnd.Next(1, totItem - 1);

            driver.FindElement(By.XPath("//li[" + ranNum + "]/h3")).Click(); // click randomly except sa last
            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//label[@for='property']")));
            System.Threading.Thread.Sleep(1000); // wait for 1 second to load

            string json = getInspections("update");
            Inspections EditInsp = JsonConvert.DeserializeObject<Inspections>(json); //get response
            Console.WriteLine("Old: ");
            Console.WriteLine(EditInsp.property + " - " + EditInsp.status + " : " + EditInsp.id);

            System.Threading.Thread.Sleep(500);

            Flds.id = EditInsp.id;
            Flds.PropertyName = "Test" + System.DateTime.Now.ToString("mm:ss");
            driver.FindElement(NgBy.Model("inspection.property")).Clear();
            driver.FindElement(NgBy.Model("inspection.property")).SendKeys(Flds.PropertyName);
            Flds.Checklist = SelectDropDown(driver, "inspection.checklist");
            Flds.Inspector = SelectDropDown(driver, "inspection.inspector");
            Flds.status = SelectDropDown(driver, "inspection.status");

            driver.FindElement(By.XPath("//button[@name='SAVE']")).Click();
            System.Threading.Thread.Sleep(3000);

            driver.FindElement(By.LinkText("Home")).Click();
            System.Threading.Thread.Sleep(3000);

           //AssertMethod(ranNum, Flds,oldFlds, driver, true);
            AssertMethod(Flds);
            
        }

        [TestCleanup]
        public void Exit()
        {
            System.Threading.Thread.Sleep(2000);
            driver.Quit();
        }

        public void AssertMethod(Fields flds)
        {
            //string json = getInspections("list");
            //List<Inspections> ListInspect = JsonConvert.DeserializeObject<List<Inspections>>(json);
            Fields tmpFlds; // store clas ng-binding
            int _id; // = Int32.Parse(idTemp);
            _id = (flds.id * 3) - 3; // to get first child of the selected inspection. 
            IList<IWebElement> ngBinding = driver.FindElements(By.ClassName("ng-binding")); // store binding to ngBinding list

            tmpFlds.id = flds.id;
            tmpFlds.PropertyName = ngBinding[_id].Text;
            tmpFlds.Checklist = ngBinding[_id + 1].Text;
            tmpFlds.Inspector= ngBinding[_id + 2].Text;
            tmpFlds.status = ngBinding[_id].Text;

            //format fields
            tmpFlds.PropertyName = tmpFlds.PropertyName.Substring(0, tmpFlds.PropertyName.LastIndexOf(' ') - 2).Trim();
            tmpFlds.Checklist = tmpFlds.Checklist.Substring(tmpFlds.Checklist.LastIndexOf(':') + 1).Trim();
            tmpFlds.Inspector = tmpFlds.Inspector.Substring(tmpFlds.Inspector.LastIndexOf(':') + 1).Trim();
            tmpFlds.status = tmpFlds.status.Substring(tmpFlds.status.LastIndexOf(' ') + 1).Trim();

            //Assert checking from json response / db
            string json = getInspections("list");
            List<Inspections> ListInspect = JsonConvert.DeserializeObject<List<Inspections>>(json);

            foreach (Inspections p in ListInspect)
            {
                if (p.id == flds.id)
                {
                    Assert.IsTrue(p.property.Equals(flds.PropertyName), "Property doesn't match on Json Response");
                    Assert.IsTrue(p.checklist.Equals(flds.Checklist), "Checklist Doesn't Match on Json Response");
                    Assert.IsTrue(p.inspector.Equals(flds.Inspector), "Inspector Doesn't Match on Json Response");
                    Assert.IsTrue(p.status.Equals(flds.status), "Status doesn't match on Json Response");

                    //Console.WriteLine(p.property);
                    //Console.WriteLine(flds.PropertyName);
                    break;
                }
            }

            // Assert as what is seen on page through class ng-binding
            Assert.IsTrue(tmpFlds.PropertyName.Equals(flds.PropertyName), "Property doesn't match on page");
            Assert.IsTrue(tmpFlds.Checklist.Equals(flds.Checklist), "Checklist Doesn't Match on page");
            Assert.IsTrue(tmpFlds.Inspector.Equals(flds.Inspector), "Inspector Doesn't Match on page");
            Assert.IsTrue(tmpFlds.status.Equals(flds.status), "Status doesn't match on page");

        }


        // Use for Assert Old
        //public void AssertMethod(int totItem, Fields flds, Fields oldflds, IWebDriver driver, Boolean Update)
        //{

        //    // IWebDriver driver;
        //    // driver = new FirefoxDriver();

        //    // Get data as seen on page
        //    string propName = driver.FindElement(By.XPath("//li[" + totItem + "]/h3[1]")).Text;
        //    propName = propName.Substring(0, propName.LastIndexOf(' ') - 2).Trim();

        //    string chcklistTemp = driver.FindElement(By.XPath("//li[" + totItem + "]/h4")).Text;
        //    chcklistTemp = chcklistTemp.Substring(chcklistTemp.LastIndexOf(':') + 1).Trim();

        //    string instpecTemp = driver.FindElement(By.XPath("//li[" + totItem + "]/h3[2]")).Text;
        //    instpecTemp = instpecTemp.Substring(instpecTemp.LastIndexOf(':') + 1).Trim();

        //    string statusTemp = driver.FindElement(By.XPath("//li[" + totItem + "]/h3[1]")).Text;
        //    statusTemp = statusTemp.Substring(statusTemp.LastIndexOf(' ') + 1).Trim();

        //    //Assert checking from json response / db
        //    string json = getInspections("list");
        //    List<Inspections> ListInspect = JsonConvert.DeserializeObject<List<Inspections>>(json);

        //    foreach (Inspections p in ListInspect)
        //    {
        //        if (p.id == flds.id)
        //        {
        //            Assert.IsTrue(p.property.Equals(flds.PropertyName), "Property doesn't match on Json Response");
        //            Assert.IsTrue(p.checklist.Equals(flds.Checklist), "Checklist Doesn't Match on Json Response");
        //            Assert.IsTrue(p.inspector.Equals(flds.Inspector), "Inspector Doesn't Match on Json Response");
        //            Assert.IsTrue(p.status.Equals(flds.status), "Status doesn't match on Json Response");

        //            //Console.WriteLine(p.property);
        //            //Console.WriteLine(flds.PropertyName);
        //            break;
        //        }
        //    }

        //    // Assert as seen on page. Could be changes with the webpage structure / design
        //    Assert.IsTrue(propName.Equals(flds.PropertyName), "Property doesn't match on page");
        //    Assert.IsTrue(chcklistTemp.Equals(flds.Checklist), "Checklist Doesn't Match on page");
        //    Assert.IsTrue(instpecTemp.Equals(flds.Inspector), "Inspector Doesn't Match on page");
        //    Assert.IsTrue(statusTemp.Equals(flds.status), "Status doesn't match on page");

        //}


        //method for selecting options in dropdowns
        public string  SelectDropDown(IWebDriver driver, string ngModelStr)
        { 
            int ctr, ranNum;
            string selected;
            Random rnd = new Random();

            ctr = new SelectElement(driver.FindElement(NgBy.Model(ngModelStr))).Options.Count;
            ranNum = rnd.Next(1, ctr); // random number with max from # of options in the dropdown list
           // Console.WriteLine("Random Number For Status: " + ranNum);
            new SelectElement(driver.FindElement(NgBy.Model(ngModelStr))).SelectByIndex(ranNum);
            // Flds.PropertyName = driver.FindElement(NgBy.Model(ngModelStr)).Text;
            IWebElement tt = driver.FindElement(NgBy.Model(ngModelStr));
            selected = new SelectElement(driver.FindElement(NgBy.Model(ngModelStr))).SelectedOption.Text;

            return selected;
        }

        public Inspections _InitCreateUpdate(Inspections insPec)
        {
            return insPec;
        }

        public string getInspections(string type)
        {

            //This is for Edit Inspection, save it to Inspectionss object. Used for single object
            //Inspections EditInsp = JsonConvert.DeserializeObject<Inspections>(json);

            //This is for Inspections list - array/List/collection
            //List<Inspections> ro = JsonConvert.DeserializeObject<List<Inspections>>(json);

            String url = driver.Url;
            if (type != "update")
            {
                if (url.Substring(url.LastIndexOf('/')) != "inspections") //add inspections to url this will load the list
                {
                    url = url + "inspections";
                }
            }
            
            var request = WebRequest.Create(url);
            string json;
            var response = (HttpWebResponse)request.GetResponse();
            request.ContentType = "application/json; charset=utf-8";
            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                json = sr.ReadToEnd();
            }
            return json;
        }

    }

 
}
